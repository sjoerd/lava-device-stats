use anyhow::{anyhow, Context, Result};
use chrono::{DateTime, TimeZone, Utc};
use lazy_static::lazy_static;
use postgres::{Client, NoTls};
use regex::Regex;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::time::Duration;

const ACTION_FLAG_CHANGED: i16 = 2;

#[derive(Copy, Clone, Debug, PartialEq)]
enum State {
    Unknown,
    Maintenance,
    Good,
    Bad,
    Looping,
    Retired,
}

impl TryFrom<&str> for State {
    type Error = anyhow::Error;
    fn try_from(v: &str) -> Result<Self, Self::Error> {
        match v {
            "Unknown" => Ok(State::Unknown),
            "Maintenance" => Ok(State::Maintenance),
            "Good" => Ok(State::Good),
            "Bad" => Ok(State::Bad),
            "Looping" => Ok(State::Looping),
            "Retired" => Ok(State::Retired),
            _ => Err(anyhow!("Unknown state: {}", v)),
        }
    }
}

#[derive(Debug)]
struct Change {
    start: DateTime<Utc>,
    state: State,
    duration: Duration,
}

fn parse_change_state(change: &str) -> Result<Option<(State, State)>> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?P<from>\w*) → (?P<to>\w*)").unwrap();
    }

    if let Some(m) = RE.captures(change) {
        // Our regexp will only match if both to and from are captured so
        // indexing directly indexing is fine.
        let from = m["from"].try_into()?;
        let to = m["to"].try_into()?;
        Ok(Some((from, to)))
    } else {
        Ok(None)
    }
}

const S_PER_HOUR: u64 = 3600;
const S_PER_DAY: u64 = 24 * S_PER_HOUR;
fn duration_to_str(d: &Duration) -> String {
    let mut s = String::new();
    let mut secs = d.as_secs();

    if secs > S_PER_DAY {
        s.push_str(&format!("{}d ", secs / S_PER_DAY));
        secs %= S_PER_DAY;
    }
    s.push_str(&format!("{:02}h", secs / S_PER_HOUR));
    s
}

fn main() -> Result<()> {
    let mut client = Client::connect("host=localhost user=postgres dbname=lavaserver", NoTls)?;

    let row = client
        .query_one(
            "SELECT id
         FROM django_content_type
         where app_label = 'lava_scheduler_app'
               and model = 'device'",
            &[],
        )
        .context("Failed to get device content id")?;
    let content_id: i32 = row.get(0);

    let starting = Utc.ymd(2018, 1, 1).and_hms(0, 0, 0);
    let rows = client
        .query(
            "SELECT *
             FROM django_admin_log
             WHERE content_type_id=$1
                and action_flag=$2
                and action_time > $3
             ORDER BY id",
            &[&content_id, &ACTION_FLAG_CHANGED, &starting],
        )
        .context("Failed to get device changes")?;

    let mut boards: HashMap<String, Vec<Change>> = HashMap::new();
    for r in rows {
        let obj: &str = r.get("object_id");
        let time: DateTime<Utc> = r.get("action_time");
        let change: &str = r.get("change_message");

        let states =
            parse_change_state(change).context(format!("Failed to parse change: {}", change))?;
        if let Some((_from, to)) = states {
            let change = Change {
                start: time,
                state: to,
                duration: Duration::new(0, 0),
            };
            if let Some(changes) = boards.get_mut(obj) {
                // We know for sure there is at least one element
                let current = changes.last_mut().unwrap();
                match (current.state, change.state) {
                    // Ignore noops
                    (f, t) if f == t => (),
                    // We should never have an unknown state
                    (State::Unknown, _) => unreachable!(),
                    // Unknown is used to trigger a healthcheck, ignore
                    (_, State::Unknown) => (),
                    // Bad after maintaince is basically more maintenance
                    (State::Maintenance, State::Bad) => (),
                    // Everything else gets recorded
                    _ => {
                        current.duration = (change.start - current.start).to_std().unwrap();
                        changes.push(change);
                    }
                }
            } else if change.state != State::Unknown {
                boards.insert(obj.to_string(), vec![change]);
            }
        }
    }

    // Drop mut
    let boards = boards;
    for (board, changes) in boards {
        let mut good = Duration::new(0, 0);
        let mut bad = Duration::new(0, 0);
        let mut maint = Duration::new(0, 0);

        let mut n_bad = 0;
        let mut n_maint = 0;

        for c in &changes {
            match c.state {
                State::Maintenance | State::Looping => {
                    maint += c.duration;
                    n_maint += 1
                }
                State::Bad => {
                    bad += c.duration;
                    n_bad += 1
                }
                State::Good => good += c.duration,
                State::Retired => (),
                State::Unknown => unreachable!(),
            }
        }

        let total = (good + bad + maint).as_secs() as f64;

        println!(
            "{:32} good: {:8} ({:2.2}%))\tbad: {:8} ({:2.2}% {}x)\tmaint: {:8} ({:2.2}% {}x)",
            board,
            duration_to_str(&good),
            good.as_secs() as f64 / total * 100.0,
            duration_to_str(&bad),
            bad.as_secs() as f64 / total * 100.0,
            n_bad,
            duration_to_str(&maint),
            maint.as_secs() as f64 / total * 100.0,
            n_maint,
        );
    }

    Ok(())
}
